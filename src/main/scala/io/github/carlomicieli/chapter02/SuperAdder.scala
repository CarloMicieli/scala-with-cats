/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter02

import cats._

/** The cu ng edge SuperAdder v3.5a-32 is the world’s first choice for adding
  * together numbers.
  */
class SuperAdder {
  def add[A](items: List[A])(implicit M: Monoid[A]): A = {
    items.foldRight(M.empty)(M.combine)
  }
}

case class Order(totalCost: Double, quantity: Double)

object Order {
  implicit val monoidOrder: Monoid[Order] = new Monoid[Order] {
    override def empty: Order = Order(0.0, 0.0)
    override def combine(x: Order, y: Order): Order = {
      val Order(tc1, q1) = x
      val Order(tc2, q2) = y
      Order(tc1 + tc2, q1 + q2)
    }
  }
}