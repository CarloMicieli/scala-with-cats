/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter03

import Printable._
import org.scalatest.{ FlatSpecLike, Matchers }

class BoxSpec extends FlatSpecLike with Matchers {
  it should "format a boolean box" in {
    format(Box(true)) shouldBe "yes"
  }

  it should "format a string box" in {
    format(Box("hello world")) shouldBe "hello world"
  }

  it should "encode a string box" in {
    Codec[Box[String]].encode(Box("hello")) shouldBe "hello"
  }

  it should "decode a string to a box" in {
    Codec[Box[String]].decode("hello") shouldBe Box("hello")
  }
}
