/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter04

import cats.syntax.either._

class EitherSpec extends ScalaCatsSpec {
  it should "create instances" in {
    3.asRight[String] shouldBe Right(3)
    "3".asLeft[Int] shouldBe Left("3")
  }

  it should "recover with values" in {
    val r: Either[String, Int] = "error".asLeft[Int].recover {
      case _: String => -1
    }
    r shouldBe Right(-1)
  }

  it should "recover with Either values" in {
    val r: Either[String, Int] = "error".asLeft[Int].recoverWith {
      case _: String => Right(-1)
    }
    r shouldBe Right(-1)
  }

  it should "check whether it satisfies a predicate" in {
    val res: Either[String, Int] = (-1).asRight[String].ensure("Must be non negative!")(_ > 0)
    res shouldBe Left("Must be non negative!")
  }

  it should "implement leftMap" in {
    val res = "foo".asLeft[Int].leftMap(_.reverse)
    res shouldBe Left("oof")

    val res2 = "foo".asRight[Int].leftMap(_ * 2)
    res2 shouldBe Right("foo")
  }

  it should "implement bimap" in {
    "bar".asLeft[Int].bimap(_.reverse, _ * 7)
  }
}
