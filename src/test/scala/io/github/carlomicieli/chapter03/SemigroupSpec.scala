/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter03

import cats._
import cats.implicits._

class SemigroupSpec extends ScalaCatsSpec {
  it should "combine two Int functions" in {
    val f = (x: Int) => x + 1
    val g = (x: Int) => x * 10

    val h: Int => Int = Semigroup[Int => Int].combine(f, g)
    h.apply(6) shouldBe 67
  }

  it should "combine two string functions" in {
    val f = (x: String) => x.toUpperCase
    val g = (x: String) => x.reverse

    val h: String => String = Semigroup[String => String].combine(f, g)
    h.apply("hello") shouldBe "HELLOolleh"
  }

  it should "combine two maps" in {
    val m1: Map[String, Int] = Map("one" -> 1)
    val m2: Map[String, Int] = Map("two" -> 2)

    val m3 = Semigroup[Map[String, Int]].combine(m1, m2)
    m3 shouldBe Map("one" -> 1, "two" -> 2)
  }

  it should "combine two maps with the same key" in {
    val m1: Map[String, Int] = Map("one" -> 1)
    val m2: Map[String, Int] = Map("one" -> 2)

    val m3 = Semigroup[Map[String, Int]].combine(m1, m2)
    m3 shouldBe Map("one" -> 3)
  }

  it should "provide Semigroup syntax" in {
    val o1: Option[Int] = Option(20)
    val o2: Option[Int] = Option(22)
    val none: Option[Int] = None

    o1 |+| o2 shouldBe Option(42)
    o1 |+| none shouldBe o1
    none |+| o2 shouldBe o2
  }
}
