/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter03

import cats.syntax.functor._
import Tree._
import org.scalatest.{ FlatSpecLike, Matchers }

class TreeSpec extends FlatSpecLike with Matchers {
  it should "map over a Leaf" in {
    val l = leaf(21)
    l.map(_ * 2) shouldBe leaf(42)
  }

  it should "map over a Tree" in {
    val l = branch(leaf(21), leaf(21))
    l.map(_ * 2) shouldBe branch(leaf(42), leaf(42))
  }

}
