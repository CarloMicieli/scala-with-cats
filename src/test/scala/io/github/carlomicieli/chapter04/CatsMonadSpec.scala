/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter04

import cats.Monad
import cats.instances.option._
import cats.instances.list._

class CatsMonadSpec extends ScalaCatsSpec {
  it should "provide an instance for Option" in {
    val opt1 = Monad[Option].pure(3)
    opt1 shouldBe Some(3)

    val opt2 = Monad[Option].flatMap(opt1)(a => Some(a + 2))
    opt2 shouldBe Some(5)
  }

  it should "provide an instance for List" in {
    val list1 = Monad[List].pure(3)
    list1 shouldBe List(3)

    val list2 = Monad[List].flatMap(List(1, 2, 3))(a => List(a, a * 10))
    list2 shouldBe List(1, 10, 2, 20, 3, 30)
  }

  it should "provide pure syntax" in {
    import cats.syntax.applicative._

    1.pure[Option] shouldBe Some(1)
    1.pure[List] shouldBe List(1)
  }

}
