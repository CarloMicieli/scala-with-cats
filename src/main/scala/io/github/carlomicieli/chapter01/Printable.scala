/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter01

trait Printable[A] {
  def format(v: A): String
}

object Printable {
  // Format the value using the corresponding instance of Printable
  def format[A](v: A)(implicit pr: Printable[A]): String = {
    pr.format(v)
  }

  // Prints the formatted value to the console
  def print[A: Printable](v: A): Unit = {
    println(format(v))
  }
}
