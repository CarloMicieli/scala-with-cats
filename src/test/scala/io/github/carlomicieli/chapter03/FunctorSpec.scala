/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter03

import cats._
import cats.instances.list._
import cats.instances.function._
import cats.instances.option._
import cats.syntax.functor._

import org.scalatest.{ FlatSpecLike, Matchers }

class FunctorSpec extends FlatSpecLike with Matchers {

  it should "map over Lists" in {
    val list1 = List(1, 2, 3)
    Functor[List].map(list1)(_ * 2) shouldBe List(2, 4, 6)
  }

  it should "map over Options" in {
    val option1 = Option(123)
    Functor[Option].map(option1)(_.toString) shouldBe Some("123")
  }

  it should "map over functions" in {
    val func1: Int => Double = (x: Int) => x.toDouble
    val func2: Double => Double = (y: Double) => y * 2

    (func1 map func2)(1) shouldEqual func1.andThen(func2)(1)
    (func1 map func2)(1) shouldEqual func2(func1(1))
  }

  it should "lift functions" in {
    val func = (x: Int) => x + 1
    val lifedFunc1 = Functor[Option].lift(func)
    val lifedFunc2 = Functor[List].lift(func)

    lifedFunc1(Option(1)) shouldBe Option(2)
    lifedFunc2(List(1, 2, 3)) shouldBe List(2, 3, 4)
  }

  it should "implement functor syntax" in {
    val f1 = (a: Int) => a + 1
    val f2 = (a: Int) => a * 2
    val f3 = (a: Int) => a + "!"
    val f4 = f1.map(f2).map(f3)

    f4(123) shouldBe "248!"
  }

  it should "compose functors" in {
    val listOpt = Functor[List] compose Functor[Option]
    listOpt.map(List(Some(1), None, Some(3)))(_ + 1) shouldBe List(Some(2), None, Some(4))
  }

  it should "pair a value with the function applied to that value" in {
    val l = List("hello", "cruel", "world")
    val product: Map[String, Int] = Functor[List].fproduct(l)(_.length).toMap

    product.get("hello") shouldBe Option(5)
  }
}
