/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter04

import MonadicDivide._

class StringByDivideSpec extends ScalaCatsSpec {
  it should "divide two valid numbers" in {
    stringByDivide("42", "2") shouldBe Some(21)
    stringByDivide2("42", "2") shouldBe Some(21)
  }

  it should "fail when either one of the value is not a number" in {
    stringByDivide("foo", "2") shouldBe None
    stringByDivide("42", "bar") shouldBe None
  }

  it should "fail when divide by 0" in {
    stringByDivide("42", "0") shouldBe None
    stringByDivide2("42", "0") shouldBe None
  }
}
