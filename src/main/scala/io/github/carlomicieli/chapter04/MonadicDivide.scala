/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter04

object MonadicDivide {
  def stringByDivide(aStr: String, bStr: String): Option[Int] = {
    parseInt(aStr).flatMap { a =>
      parseInt(bStr).flatMap { b =>
        divide(a, b)
      }
    }
  }

  def stringByDivide2(aStr: String, bStr: String): Option[Int] = {
    for {
      a <- parseInt(aStr)
      b <- parseInt(bStr)
      div <- divide(a, b)
    } yield div
  }

  def parseInt(str: String): Option[Int] = {
    scala.util.Try(str.toInt).toOption
  }

  def divide(a: Int, b: Int): Option[Int] = {
    if (b == 0) None else Some(a / b)
  }
}
