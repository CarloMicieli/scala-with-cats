/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter01

import cats._
import java.util.Date

object ShowDate1 {
  implicit val showDate: Show[Date] = new Show[Date] {
    def show(date: Date): String =
      s"${date.getTime}ms since the epoch."
  }
}

// using the methods provided by cats.Show
object ShowDate2 {
  implicit val showDate: Show[Date] =
    Show.show(date => s"${date.getTime}ms since the epoch.")
}
