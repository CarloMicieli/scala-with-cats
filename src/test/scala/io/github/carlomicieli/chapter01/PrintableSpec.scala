/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter01

import org.scalatest.{ FlatSpecLike, Matchers }

class PrintableSpec extends FlatSpecLike with Matchers {

  it should "format an Int value" in {
    import PrintableInstances._
    Printable.format(42) shouldBe "42"
  }

  it should "format a String value" in {
    import PrintableInstances._
    Printable.format("hello cats") shouldBe "hello cats"
  }

  it should "format a cat object" in {
    val cat = Cat("Garfield", 42, "orange")
    Printable.format(cat) shouldBe "Garfield is a 42 year-old orange cat."
  }

  it should "format a cat object using the syntax" in {
    import PrintableSyntax._

    val cat = Cat("Garfield", 42, "orange")
    cat.format shouldBe "Garfield is a 42 year-old orange cat."
  }
}
