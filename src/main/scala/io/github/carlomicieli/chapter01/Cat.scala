/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter01

import cats.{ Eq, Show }
import cats.instances.int._
import cats.instances.string._

final case class Cat(name: String, age: Int, color: String)

object Cat {
  implicit val catsEq: Eq[Cat] = (x: Cat, y: Cat) => x.equals(y)

  implicit val catsShow: Show[Cat] = Show.show((c: Cat) => {
    val name = Show[String].show(c.name)
    val age = Show[Int].show(c.age)
    val color = Show[String].show(c.color)

    s"$name is a $age year-old $color cat."
  })

  implicit val catsPrintable: Printable[Cat] = {
    (c: Cat) =>
      {
        import PrintableInstances._
        import Printable._

        val name = format(c.name)
        val age = format(c.age)
        val color = format(c.color)

        s"$name is a $age year-old $color cat."
      }
  }
}