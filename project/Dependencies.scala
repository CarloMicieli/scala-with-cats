import sbt._

object Version {
  final val ScalaTest  = "3.0.3"
  final val ScalaCheck = "1.13.4"
  final val Cats       = "1.0.1"
}

object Dependencies {
  val cats       = "org.typelevel" %% "cats-core"   % Version.Cats
  val catsLaws   = "org.typelevel" %% "cats-laws"   % Version.Cats
  val scalaTest  = "org.scalatest"  %% "scalatest"  % Version.ScalaTest
  val scalaCheck = "org.scalacheck" %% "scalacheck" % Version.ScalaCheck
  val discipline = "org.typelevel" %% "discipline" % "0.8"
}

object Scalac {
  final val `2.12.4`: String = "2.12.4"
}
