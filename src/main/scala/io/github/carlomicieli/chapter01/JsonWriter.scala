/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter01

// The serialize to JSON behaviour is encoded in this trait
trait JsonWriter[A] {
  def write(v: A): Json
}

object JsonSyntax {
  implicit class JsonWriterOps[A](val value: A) extends AnyVal {
    def toJson(implicit jw: JsonWriter[A]): Json = {
      jw.write(value)
    }
  }
}