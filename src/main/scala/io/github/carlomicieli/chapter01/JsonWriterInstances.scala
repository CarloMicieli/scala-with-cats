/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter01

final case class Person(name: String, email: String)

// Instances for the JsonWriter typeclass
object JsonWriterInstances {
  implicit val intJsonWriter: JsonWriter[Int] = {
    (v: Int) => JsNumber(v.toDouble)
  }

  implicit val stringJsonWriter: JsonWriter[String] = {
    (v: String) => JsString(v)
  }

  implicit val personJsonWriter: JsonWriter[Person] = {
    (p: Person) =>
      {
        JsObject(Map(
          "name" -> JsString(p.name),
          "email" -> JsString(p.email)))
      }
  }

  implicit def optionJsonWriter[A](implicit jw: JsonWriter[A]): JsonWriter[Option[A]] = {
    (o: Option[A]) =>
      {
        o match {
          case Some(v) => jw.write(v)
          case None    => JsNull
        }
      }
  }
}
