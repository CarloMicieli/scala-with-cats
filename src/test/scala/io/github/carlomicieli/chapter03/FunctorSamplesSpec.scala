/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter03

import cats.instances.option._
import cats.instances.list._
import FunctorSamples._
import org.scalatest.{ FlatSpecLike, Matchers }

class FunctorSamplesSpec extends FlatSpecLike with Matchers {
  it should "apply doMath to Options" in {
    doMath(Option(20)) shouldBe Option(22)
  }

  it should "apply doMath to Lists" in {
    doMath(List(1, 2, 3)) shouldBe List(3, 4, 5)
  }
}
