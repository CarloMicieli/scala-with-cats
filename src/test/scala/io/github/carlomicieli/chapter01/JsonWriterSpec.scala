/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter01

import org.scalatest.{ FlatSpecLike, Matchers }

class JsonWriterSpec extends FlatSpecLike with Matchers {

  it should "write Json from Person" in {
    import JsonWriterInstances._

    val json = Json.toJson(dave)
    json shouldBe expectedJsObject
  }

  it should "write Json from Person using the syntax" in {
    import JsonWriterInstances._
    import JsonSyntax._

    val json = dave.toJson
    json shouldBe expectedJsObject
  }

  it should "write Json from Person using implicitly" in {
    import JsonWriterInstances._

    val jsonWriter = implicitly[JsonWriter[Person]]

    val json = jsonWriter.write(dave)
    json shouldBe expectedJsObject
  }

  it should "write a Json value from Option" in {
    import JsonWriterInstances._

    val some: Option[Int] = Some(42)
    val none: Option[Int] = None

    val jsonSome = Json.toJson(some)
    val jsonNone = Json.toJson(none)

    jsonNone shouldBe JsNull
    jsonSome shouldBe JsNumber(42.0)
  }

  val dave: Person = Person("dave", "dave@mail.com")

  val expectedJsObject: JsObject = {
    JsObject(Map(
      "name" -> JsString("dave"),
      "email" -> JsString("dave@mail.com")))
  }
}
