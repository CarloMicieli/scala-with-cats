/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter01

import java.util.Date

import org.scalatest.{ FlatSpecLike, Matchers }
import cats._
import cats.instances.int._
import cats.instances.option._

class CatsEqSpec extends FlatSpecLike with Matchers {
  val cat1 = Cat("Garfield", 38, "orange and black")
  val cat2 = Cat("Heathcliff", 33, "orange and black")

  it should "check equality for Int values" in {
    Eq[Int].eqv(123, 123) shouldBe true
    Eq[Int].eqv(123, 234) shouldBe false
  }

  it should "check equality for Int values, using syntax" in {
    import IntEqSyntax._
    testEqSyntax shouldBe true
    testNeqSyntax shouldBe true
  }

  it should "check equality for Date values" in {
    implicit val dateEq: Eq[Date] =
      Eq.instance[Date] { (date1, date2) =>
        date1.getTime === date2.getTime
      }

    val d1 = new Date(1514569244867L)
    val d2 = new Date(1514569244866L)

    Eq[Date].eqv(d1, d2) shouldBe false
  }

  it should "check equality for 2 Cats" in {
    Eq[Cat].eqv(cat1, cat2) shouldBe false
    Eq[Cat].neqv(cat1, cat2) shouldBe true
  }

  it should "check equality for Cats inside Option" in {
    val optCat1 = Option(cat1)
    val optCat2 = Option(cat2)
    Eq[Option[Cat]].eqv(optCat1, optCat1) shouldBe true
    Eq[Option[Cat]].neqv(optCat1, optCat2) shouldBe true
  }
}

object IntEqSyntax {
  def testEqSyntax: Boolean = {
    import cats.syntax.eq._
    123 === 123
  }

  def testNeqSyntax: Boolean = {
    import cats.syntax.eq._
    123 =!= 234
  }
}
