# Scala with Cats

![Cover](img/scala-with-cats.png)

## Type classes

### Chapter 1

```scala
trait Show[A] {
  def show(x: A): String
}
```

```scala
trait Eq[A] {
  def eqv(x: A, y: A): Boolean
  def neqv(x: A, y: A): Boolean
}
```

### Chapter 2: Monoids and Semigroups

```scala
trait Semigroup[A] {
  def combine(x: A, y: A): A
}

trait Monoid[A] extends Semigroup[A] {
  def empty: A
}
```

Monoid laws:
* _associative law_
* _identity law_