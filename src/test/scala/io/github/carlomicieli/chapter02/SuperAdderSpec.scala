/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter02

import cats.instances.int._
import cats.instances.option._
import org.scalatest.{ FlatSpecLike, Matchers }

class SuperAdderSpec extends FlatSpecLike with Matchers {
  val adder: SuperAdder = new SuperAdder

  it should "sum a list of numbers" in {
    adder.add(List(1, 2, 3, 4, 5)) shouldBe 15
  }

  it should "sum a list of options" in {
    val items: List[Option[Int]] = List(Some(1), Some(2), Some(3), Some(4), Some(5))
    adder.add(items) shouldBe Some(15)
  }

  it should "sum a list of orders" in {
    val items: List[Order] = List(Order(15.0, 1.0), Order(2.5, 4.0), Order(3.0, 2.0))
    adder.add(items) shouldBe Order(20.5, 7.0)
  }
}
