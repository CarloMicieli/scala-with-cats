/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter04

import cats.instances.option._
import cats.instances.list._

class sumSquareSpec extends ScalaCatsSpec {
  it should "sum Option square" in {
    sumSquare(Option(3), Option(4)) shouldBe Some(25)
  }

  it should "sum List square" in {
    sumSquare(List(1, 2, 3), List(4, 5)) shouldBe List(17, 26, 20, 29, 25, 34)
  }

  it should "sum plain value square" in {
    import cats.Id

    sumSquare(3: Id[Int], 4: Id[Int]) shouldBe (25: Id[Int])
  }
}
