/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter03

final case class Box[A](value: A)

object Box {
  implicit final def printableBox[A](implicit pa: Printable[A]): Printable[Box[A]] = {
    pa.contramap((b: Box[A]) => b.value)
  }

  implicit final def codecBox[A](implicit ca: Codec[A]): Codec[Box[A]] = {
    ca.imap(Box.apply, _.value)
  }
}
