/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli.chapter03

import cats._

sealed trait Tree[+A]
final case class Branch[A] private (left: Tree[A], right: Tree[A]) extends Tree[A]
final case class Leaf[A] private (value: A) extends Tree[A]

object Tree {
  def leaf[A](x: A): Tree[A] = Leaf(x)
  def branch[A](l: Tree[A], r: Tree[A]): Tree[A] = Branch(l, r)

  implicit final def treeFunctor: Functor[Tree] = new Functor[Tree] {
    override def map[A, B](fa: Tree[A])(f: A => B): Tree[B] = {
      fa match {
        case Branch(l, r) => Branch(map(l)(f), map(r)(f))
        case Leaf(v)      => Leaf(f(v))
      }
    }
  }
}
