/*
 * Copyright 2018 Carlo Micieli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.carlomicieli
package chapter02

import cats._
import cats.instances.string._
import cats.instances.int._
import cats.instances.option._
import cats.instances.tuple._
import org.scalatest.{ FlatSpecLike, Matchers }

class MonoidSpec extends FlatSpecLike with Matchers {
  it should "combine two strings" in {
    Monoid[String].combine("Hi ", "there") shouldBe "Hi there"
  }

  it should " produce the empty string" in {
    Monoid[String].empty shouldBe ""
  }

  it should " combine two strings via Semigroup" in {
    Semigroup[String].combine("Hi ", "there") shouldBe "Hi there"
  }

  it should " combine two strings via syntax" in {
    import cats.syntax.semigroup._

    val stringResult = "Hi " |+| "there" |+| Monoid[String].empty
    stringResult shouldBe "Hi there"

    val intResult = 1 |+| 2 |+| Monoid[Int].empty
    intResult shouldBe 3
  }

  it should " combine two pairs" in {
    val expected = (0, 0)
    Monoid[(Int, Int)].empty shouldBe expected
    Monoid[(Int, Int)].combine((1, 2), (2, 1)) shouldBe ((3, 3))
  }

  it should " combine two Int" in {
    Monoid[Int].combine(21, 21) shouldBe 42
  }

  it should " combine two Option values" in {
    val a: Option[Int] = Some(41)
    val b: Option[Int] = Some(1)
    Monoid[Option[Int]].combine(a, b) shouldBe Some(42)
  }
}
